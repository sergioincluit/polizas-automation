webdriver_manager
selenium~=3.141.0
pytest~=6.0.1
pytest-bdd~=3.4.0
pytest-html
python-dateutil~=2.8.1
configparser~=5.0.2

sphinx
sphinx_rtd_theme
sphinx-autopackagesummary

allure-pytest==2.8.13
assertpy
PyPDF4