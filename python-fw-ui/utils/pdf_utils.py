from PyPDF4 import PdfFileReader


class UtilsPDF:

    def return_text_of_pdf(pdf_path, num_page):
        with open(pdf_path, 'rb') as f:
            pdf = PdfFileReader(f)
            page = pdf.getPage(num_page)
            return page.extractText()

    def return_number_of_pages(pdf_path):
        with open(pdf_path, 'rb') as f:
            pdf = PdfFileReader(f)
            return pdf.getNumPages()
