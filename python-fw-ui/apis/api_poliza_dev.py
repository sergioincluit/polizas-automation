import logging
import requests


class ApiPolizaDEV:
    def __init__(self, username, password):
        logging.info(f"Definiendo el constructor de Pólizas, con usuario: {username} y password: {password} en dev")
        self.base_url = "http://polizas.dev.azure.ma/"
        self.token = self.__generar_token(username, password)

    def __generar_token(self, username, password):
        payload = {
            'username': username,
            'password': password,
            'client_id': 'spa-sima-web',
            'grant_type': 'password'
        }
        logging.info(f"Intentando hacer post a la api:{'http://idm.dev.azure.ma/auth/realms/meran/protocol/openid-connect/token'} en dev")
        response = requests.post(url='http://idm.dev.azure.ma/auth/realms/meran/protocol/openid-connect/token', headers={}, data=payload,
                                 auth=(username,password))
        if response.status_code == 200:
            logging.info("Se completo la creación del token en dev")
            return response.json()["access_token"]
        else:
            logging.error(f"Fallo la creación del token {response.status_code} en dev")
            return False

    def get_token(self):
        return self.token

    def get_buscar_poliza_por_numero(self, poliza):
        path = f"consulta/polizas/{poliza}"
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': f'Bearer {self.token}'
        }
        logging.info(f"Intentando hacer get a la api:{self.base_url + path} en dev")
        response = requests.get(url=self.base_url + path, headers=headers, data={})
        logging.info("Finalizo el proceso get a la API en dev")
        return response