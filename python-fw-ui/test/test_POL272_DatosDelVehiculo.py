import time

import allure
import pytest

from pages.buscar_poliza_page import BuscarPolizasPage
from pages.detalles_polizas_page import DetallesPolizasPage
from pages.polizas_page import PolizasPage
from pages.riesgo_poliza import RisegoPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('POLIZAS'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('Buscar póliza')
]


@pytest.mark.POLIZAS
@pytest.mark.regression
class TestDatosDelVehiculo:
    """
    Automatizacion del siguiente test case: https://lamercantil.atlassian.net/browse/POL-264
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_visualizar_datos_del_vehiculo(self, open_login_page_POLIZAS):
        """
        Test: Consultar los datos del vehiculo en cada póliza.

        Precondiciones:
            - Usuario con acceso al formulario Pólizas en EKO, ingresar poliza, seleccionar póliza.

        Steps:
            - Portal EKO --> Opcion Poliza --> Ingrese su busqueda → Buscar --> Seleccionar poliza →
            desplazarse hasta el final → Seleccionar el boton de ver → selecionar el signo > para desplegar los datos

        Resultado esperado:
            - Vizualizar los campos referentes al vehiculo entre ellos
                Item:1
                Descripcion:MERC.BENZ VITO 111 CDI FURGON V2 AA
                Suma Asegurada:935000
                Valido Desde:2018-12-14
                Valido Hasta:2019-01-11
                Ubicacion:
                Localidad:ITUZAINGO
                Codigo Postal:1714
                Datos de Vehiculo
                Codigo:3268912
                InfoAuto:280543
                Modelo:MERC.BENZ VITO 111 CDI FURGON V2 AA
                Fabricado:2016
                Dominio:PLU414
                Vin:8AB447603GE826239
                Motor:R9MA502C014041
                Codigo Tipo:3
                Tipo:PICK UP
                Codigo Carroceria:7
                Tipo Carroceria:FURGON
                Codigo Uso:1
                Uso:PARTICULAR
                Gnc:N

                Mostrar la Zona de RC y Casco
        """
        login_page = open_login_page_POLIZAS
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        polizas = PolizasPage()
        assert polizas.is_displayed()
        polizas.seleccionar_menu_poliza()
        buscar_poliza = BuscarPolizasPage()
        assert buscar_poliza.is_displayed()
        buscar_poliza.buscar_poliza_por_numero("510693342")
        buscar_poliza.seleccionar_buscar()
        assert buscar_poliza.obtener_datos_grilla() % 9 == 0, "Debería ser divisible en 9"
        buscar_poliza.seleccionar_poliza()
        detalle_poliza = DetallesPolizasPage()
        assert detalle_poliza.is_displayed()
        detalle_poliza.visualizar_datos_del_vehiculo()
        riesgo = RisegoPage()
        assert riesgo.is_displayed()
        riesgo.seleccionar_detalle_de_poliza()
        riesgo.validar_riesgo()
        time.sleep(3)
