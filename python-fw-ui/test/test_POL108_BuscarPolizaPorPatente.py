import time

import allure
import pytest

from pages.buscar_poliza_page import BuscarPolizasPage
from pages.polizas_page import PolizasPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('POLIZAS'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('Buscar póliza')
]


@pytest.mark.POLIZAS
@pytest.mark.regression
class TestBuscarPolizaPorPatente:
    """
    Automatizacion del siguiente test case: https://lamercantil.atlassian.net/browse/POL-108
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_buscar_polizas_por_numero_de_patente_antiguo(self, open_login_page_POLIZAS):
        """
        Test: Buscar una póliza por su número
            Verificar que al ingresar un número de patente existente se listen los endosos de la misma en la grilla.

        Precondiciones:
            - Usuario con acceso al formulario Pólizas en EKO. Además la póliza debe existir y tener endosos.

        Steps:
            - Portal EKO --> Opción Póliza --> Buscar póliza

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                *Nro de póliza
                *Nombre / Razón Social (VER)
                *Orden
                *Tipo de movimiento
                *Asegurado
                *Vigencia (Desde - Hasta)
                *Fecha emisión (VER)
                *Premio (VER)
                *Tipo renovación (A o M) (VER)
                *Nro de Item
        """
        login_page = open_login_page_POLIZAS
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        polizas = PolizasPage()
        assert polizas.is_displayed()
        polizas.seleccionar_menu_poliza()
        buscar_poliza = BuscarPolizasPage()
        assert buscar_poliza.is_displayed()
        buscar_poliza.buscar_poliza_por_patente("ANP452")
        buscar_poliza.seleccionar_buscar()
        assert buscar_poliza.obtener_datos_grilla() % 10 == 0, "Debería ser divisible en 10"
        buscar_poliza.valida_datos_de_poliza_buscada_por_patente()
        time.sleep(3)

    def test_buscar_polizas_por_numero_de_patente_mercosur(self, open_login_page_POLIZAS):
        """
        Test: Buscar una póliza por su número
            Verificar que al ingresar un número de patente existente se listen los endosos de la misma en la grilla.

        Precondiciones:
            - Usuario con acceso al formulario Pólizas en EKO. Además la póliza debe existir y tener endosos.

        Steps:
            - Portal EKO --> Opción Póliza --> Buscar póliza

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                *Nro de póliza
                *Nombre / Razón Social (VER)
                *Orden
                *Tipo de movimiento
                *Asegurado
                *Vigencia (Desde - Hasta)
                *Fecha emisión (VER)
                *Premio (VER)
                *Tipo renovación (A o M) (VER)
                *Nro de Item
        """
        login_page = open_login_page_POLIZAS
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        polizas = PolizasPage()
        assert polizas.is_displayed()
        polizas.seleccionar_menu_poliza()
        buscar_poliza = BuscarPolizasPage()
        assert buscar_poliza.is_displayed()
        buscar_poliza.buscar_poliza_por_patente("AD512DS")
        buscar_poliza.seleccionar_buscar()
        assert buscar_poliza.obtener_datos_grilla() % 10 == 0, "Debería ser divisible en 10"
        buscar_poliza.valida_datos_de_poliza_buscada_por_patente(True)
        time.sleep(3)
