import time

import allure
import pytest

from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('POLIZAS'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('Login')
]


@pytest.mark.POLIZAS
@pytest.mark.regression
class TestLogin:
    """
    Automatizacion del siguiente test case: https://lamercantil.atlassian.net/browse/POL-217
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_verify_home_page_is_open(self, open_login_page_POLIZAS):
        """
        Test: Verificar elementos en la pagina de login
            El objetivo del test es verificar que se muestren todos los elementos requeridos en la pagina de login y que tengan los textos esperados

        Precondiciones:
            - El usuario tiene que estar en la pagina de login

        Steps:
            - Visualizar la pantalla de login

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                - Campo para ingresar el usuario con el placeholder "email"
                - Campo para ingresar contraseña con el placeholder "Contraseña"
                - Boton para hacer visible la contraseña
                - Boton para iniciar sesión con el texto "Iniciar sesión"
        """
        login_page = open_login_page_POLIZAS
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        time.sleep(5)