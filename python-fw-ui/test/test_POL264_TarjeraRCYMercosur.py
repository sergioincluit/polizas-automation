import time

import allure
import pytest

from pages.buscar_poliza_page import BuscarPolizasPage
from pages.detalles_polizas_page import DetallesPolizasPage
from pages.polizas_page import PolizasPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('POLIZAS'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('Buscar póliza')
]


@pytest.mark.POLIZAS
@pytest.mark.regression
class TestTarjeraRCYMercosur:
    """
    Automatizacion del siguiente test case: https://lamercantil.atlassian.net/browse/POL-264
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_descargar_tarjeraRC_y_mercosur(self, open_login_page_POLIZAS):
        """
        Test: Consultar bienes asegurados en cada póliza.

        Precondiciones:
            - Usuario con acceso al formulario Pólizas en EKO, ingresar poliza, seleccionar póliza.

        Steps:
            - Portal EKO --> Opcion Poliza --> Ingrese su busqueda → Buscar --> Seleccionar poliza → desplazarse hasta el final → Seleccionar el boton de descargar

        Resultado esperado:
            - Se descarga un PDF donde se visualiza en la primer página la Tarjeta RC y en la segunda la de Mercosur
        """
        login_page = open_login_page_POLIZAS
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        polizas = PolizasPage()
        assert polizas.is_displayed()
        polizas.seleccionar_menu_poliza()
        buscar_poliza = BuscarPolizasPage()
        assert buscar_poliza.is_displayed()
        buscar_poliza.buscar_poliza_por_numero("510693342")
        buscar_poliza.seleccionar_buscar()
        assert buscar_poliza.obtener_datos_grilla() % 9 == 0, "Debería ser divisible en 9"
        buscar_poliza.seleccionar_poliza()
        detalle_poliza = DetallesPolizasPage()
        assert detalle_poliza.is_displayed()
        detalle_poliza.descargar_pdf()
        time.sleep(3)
