import logging

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class PolizasPage(BasePage):

    __USER_LOGGED_LBL = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Pólizas')]"
    })

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__USER_LOGGED_LBL)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla del Home Page.")
            return False

    def seleccionar_menu_poliza(self):
        self.wait_for_element(self.__USER_LOGGED_LBL)
        self.click_element(self.__USER_LOGGED_LBL)
        self.wait_for_invisibility_of_element(self.__SPINNER)


