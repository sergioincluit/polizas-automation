import logging

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class HomePagePolizas(BasePage):

    __USER_LOGGED_LBL = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Pólizas')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__USER_LOGGED_LBL)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla del Home Page.")
            return False
