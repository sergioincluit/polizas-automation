import logging

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class RisegoPage(BasePage):

    __RIESGO_PAGE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Riesgo')]"
    })

    __SELECCIONAR_DETALLE_DE_POLIZA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "(//div[@class='p-accordion p-component']//a[contains(@class,'p-accordion-header-link')]//span[contains(@class,'pi-fw pi-chevron-right')])[1]"
    })

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    __ITEM_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'itemText'
    })

    __ITEM_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'itemValue'
    })

    __DESCRIPCION_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'descText'
    })

    __DESCRIPCION_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'descValue'
    })

    __SUMA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'sumaText'
    })

    __SUMA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'sumaValue'
    })

    __VALIDO_DESDE_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'desdeText'
    })

    __VALIDO_DESDE_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'desdeValue'
    })

    __VALIDO_HASTA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'hastaText'
    })

    __VALIDO_HASTA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'hastaValue'
    })

    __UBICACION_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'ubiText'
    })

    __UBICACION_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'ubiValue'
    })

    __LOCALIDAD_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'localidadText'
    })

    __LOCALIDAD_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'localidadValue'
    })

    __CODIGO_POSTAL_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'postalText'
    })

    __CODIGO_POSTAL_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'postalValue'
    })

    __CODIGO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codText'
    })

    __CODIGO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codValue'
    })

    __INFOAUTO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'infoAutoText'
    })

    __INFOAUTO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'infoAutoValue'
    })

    __MODELO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'modeloText'
    })

    __MODELO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'modeloValue'
    })

    __FABRICADO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'fabriText'
    })

    __FABRICADO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'fabriValue'
    })

    __DOMINIO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'dominioText'
    })

    __DOMINIO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'dominioValue'
    })

    __VIN_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'vinText'
    })

    __VIN_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'vinValue'
    })

    __MOTOR_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'motorText'
    })

    __MOTOR_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'motorValue'
    })

    __CODIGO_TIPO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codTipoText'
    })

    __CODIGO_TIPO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codTipoValue'
    })

    __TIPO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoText'
    })

    __TIPO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoValue'
    })

    __CODIGO_CARROCERIA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codCarroceriaText'
    })

    __CODIGO_CARROCERIA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codCarroceriaValue'
    })

    __TIPO_CARROCERIA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoCarroceriaText'
    })

    __TIPO_CARROCERIA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoCarroceriaValue'
    })

    __CODIGO_USO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codUsoText'
    })

    __CODIGO_USO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'codUsoValue'
    })

    __USO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'usoText'
    })

    __USO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'usoValue'
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_1009 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 1009')]"
    })

    __DESCRIPCION_COBERTURA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'descripcionCoberturaText'
    })

    __DESCRIPCION_COBERTURA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'descripcionCoberturaValue'
    })

    __TIPO_COBERTURA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoCoberturaText'
    })

    __TIPO_COBERTURA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tipoCoberturaValue'
    })

    __LIMITE_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'limiteText'
    })

    __LIMITE_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'limiteValue'
    })

    __FRANQUICIA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'franquiciaText'
    })

    __FRANQUICIA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'franquiciaValue'
    })

    __SUMA_ACCESORIO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'sumaAccText'
    })

    __SUMA_ACCESORIO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'sumaAccValue'
    })

    __CODIGO_9013 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 9013')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_9018 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 9018')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_9022 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 9022')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_9029 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 9029')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_9063 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 9063')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    __CODIGO_2063 = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Codigo: 2063')]"
    })

    __GNC_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncText'
    })

    __GNC_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gncValue'
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__RIESGO_PAGE)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla Riesgo.")
            return False

    def seleccionar_detalle_de_poliza(self):
        #self.wait_for_invisibility_of_element(self.__SPINNER)
        self.wait_for_element(self.__SELECCIONAR_DETALLE_DE_POLIZA)
        self.click_element(self.__SELECCIONAR_DETALLE_DE_POLIZA)

    def validar_riesgo(self):
        self.scroll_to_element(self.__ITEM_TEXT)
        self.wait_for_element(self.__ITEM_TEXT)
        assert self.is_text_equal(self.get_text_element(self.__ITEM_TEXT), "Item:"), \
            f"El titulo 'Item:' no coincide con {self.get_text_element(self.__ITEM_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__ITEM_VALUE), "1"), \
            f"El valor del Item '1' no coincide con " \
            f"{self.get_text_element(self.__ITEM_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__DESCRIPCION_TEXT), "Descripción:"), \
            f"El titulo 'Descripción:' no coincide con " \
            f"{self.get_text_element(self.__DESCRIPCION_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__DESCRIPCION_VALUE), "MERC.BENZ VITO 111 CDI FURGON V2 AA"), \
            f"El valor de la Descripción 'MERC.BENZ VITO 111 CDI FURGON V2 AA' no coincide con " \
            f"{self.get_text_element(self.__DESCRIPCION_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__SUMA_TEXT), "Suma Asegurada:"), \
            f"El titulo 'Suma Asegurada:' no coincide con " \
            f"{self.get_text_element(self.__SUMA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__SUMA_VALUE), "935000"), \
            f"El valor de la Suma Asegurada '935000' no coincide con " \
            f"{self.get_text_element(self.__SUMA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__VALIDO_DESDE_TEXT), "Valido Desde:"), \
            f"El titulo 'Valido Desde:' no coincide con " \
            f"{self.get_text_element(self.__VALIDO_DESDE_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__VALIDO_DESDE_VALUE), "2018-12-14"), \
            f"El valor de Valido Desde '2018-12-14' no coincide con " \
            f"{self.get_text_element(self.__VALIDO_DESDE_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__VALIDO_HASTA_TEXT), "Valido Hasta:"), \
            f"El titulo 'Valido Hasta:' no coincide con " \
            f"{self.get_text_element(self.__VALIDO_HASTA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__VALIDO_HASTA_VALUE), "2019-01-11"), \
            f"El valor de Valido Hasta '2019-01-11' no coincide con " \
            f"{self.get_text_element(self.__VALIDO_HASTA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__UBICACION_TEXT), "Ubicacion:"), \
            f"El titulo 'Ubicacion:' no coincide con " \
            f"{self.get_text_element(self.__UBICACION_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__UBICACION_VALUE), ""), \
            f"El valor de Ubicacion '' no coincide con " \
            f"{self.get_text_element(self.__UBICACION_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__LOCALIDAD_TEXT), "Localidad:"), \
            f"El titulo 'Localidad:' no coincide con " \
            f"{self.get_text_element(self.__LOCALIDAD_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__LOCALIDAD_VALUE), "ITUZAINGO"), \
            f"El valor de Localidad 'ITUZAINGO' no coincide con " \
            f"{self.get_text_element(self.__LOCALIDAD_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__CODIGO_POSTAL_TEXT), "Código Postal:"), \
            f"El titulo 'Código Postal:' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_POSTAL_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__CODIGO_POSTAL_VALUE), "1714"), \
            f"El valor de Código Postal '1714' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_POSTAL_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__CODIGO_TEXT), "Código:"), \
            f"El titulo 'Código:' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__CODIGO_VALUE), "3268912"), \
            f"El valor de Código '3268912' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__INFOAUTO_TEXT), "InfoAuto:"), \
            f"El titulo 'InfoAuto:' no coincide con " \
            f"{self.get_text_element(self.__INFOAUTO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__INFOAUTO_VALUE), "280543"), \
            f"El valor de InfoAuto '280543' no coincide con " \
            f"{self.get_text_element(self.__INFOAUTO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__MODELO_TEXT), "Modelo:"), \
            f"El titulo 'Modelo:' no coincide con " \
            f"{self.get_text_element(self.__MODELO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__MODELO_VALUE), "MERC.BENZ VITO 111 CDI FURGON V2 AA"), \
            f"El valor de Modelo 'MERC.BENZ VITO 111 CDI FURGON V2 AA' no coincide con " \
            f"{self.get_text_element(self.__MODELO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__FABRICADO_TEXT), "Fabricado:"), \
            f"El titulo 'Fabricado:' no coincide con " \
            f"{self.get_text_element(self.__FABRICADO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__FABRICADO_VALUE), "2016"), \
            f"El valor de Fabricado '2016A' no coincide con " \
            f"{self.get_text_element(self.__FABRICADO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__DOMINIO_TEXT), "Dominio:"), \
            f"El titulo 'Dominio:' no coincide con " \
            f"{self.get_text_element(self.__DOMINIO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__DOMINIO_VALUE), "PLU414"), \
            f"El valor de Dominio 'PLU414' no coincide con " \
            f"{self.get_text_element(self.__DOMINIO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__VIN_TEXT), "Vin:"), \
            f"El titulo 'Vin:' no coincide con " \
            f"{self.get_text_element(self.__VIN_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__VIN_VALUE), "8AB447603GE826239"), \
            f"El valor de Vin '8AB447603GE826239' no coincide con " \
            f"{self.get_text_element(self.__VIN_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__MOTOR_TEXT), "Motor:"), \
            f"El titulo 'Motor:' no coincide con " \
            f"{self.get_text_element(self.__MOTOR_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__MOTOR_VALUE), "R9MA502C014041"), \
            f"El valor de Motor 'R9MA502C014041' no coincide con " \
            f"{self.get_text_element(self.__MOTOR_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__CODIGO_TIPO_TEXT), "Código Tipo:"), \
            f"El titulo 'Código Tipo:' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_TIPO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__CODIGO_TIPO_VALUE), "3"), \
            f"El valor de Código Tipo '3' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_TIPO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__TIPO_TEXT), "Tipo:"), \
            f"El titulo 'Tipo:' no coincide con " \
            f"{self.get_text_element(self.__TIPO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__TIPO_VALUE), "PICK UP"), \
            f"El valor de Tipo 'PICK UP' no coincide con " \
            f"{self.get_text_element(self.__TIPO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__CODIGO_CARROCERIA_TEXT), "Código Carroceria:"), \
            f"El titulo 'Código Carroceria:' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_CARROCERIA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__CODIGO_CARROCERIA_VALUE), "7"), \
            f"El valor de Código Carroceria '7' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_CARROCERIA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__CODIGO_USO_TEXT), "Código Uso:"), \
            f"El titulo 'Código Uso:' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_USO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__CODIGO_USO_VALUE), "1"), \
            f"El valor de Código Uso '1' no coincide con " \
            f"{self.get_text_element(self.__CODIGO_USO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__USO_TEXT), "Uso:"), \
            f"El titulo 'Uso:' no coincide con " \
            f"{self.get_text_element(self.__USO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__USO_VALUE), "PARTICULAR"), \
            f"El valor de Uso 'PARTICULAR' no coincide con " \
            f"{self.get_text_element(self.__USO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__GNC_TEXT), "Gnc:"), \
            f"El titulo 'Gnc:' no coincide con " \
            f"{self.get_text_element(self.__GNC_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__GNC_VALUE), "N"), \
            f"El valor de Gnc: 'N' no coincide con " \
            f"{self.get_text_element(self.__GNC_VALUE)}"

        self.wait_for_element(self.__CODIGO_1009)
        self.click_element(self.__CODIGO_1009)
        self.scroll_to_element(self.__CODIGO_2063)
        self.wait_for_element(self.__CODIGO_2063)
        self.click_element(self.__CODIGO_2063)
        self.scroll_to_element(self.__CODIGO_9013)
        self.wait_for_element(self.__CODIGO_9013)
        self.click_element(self.__CODIGO_9013)
        self.scroll_to_element(self.__CODIGO_9018)
        self.wait_for_element(self.__CODIGO_9018)
        self.click_element(self.__CODIGO_9018)
        self.scroll_to_element(self.__CODIGO_9022)
        self.wait_for_element(self.__CODIGO_9022)
        self.click_element(self.__CODIGO_9022)
        self.scroll_to_element(self.__CODIGO_9029)
        self.wait_for_element(self.__CODIGO_9029)
        self.click_element(self.__CODIGO_9029)
        self.scroll_to_element(self.__CODIGO_9063)
        self.wait_for_element(self.__CODIGO_9063)
        self.click_element(self.__CODIGO_9063)
        self.wait_for_element(self.__DESCRIPCION_COBERTURA_VALUE)
        self.descripcion_cobertura = self.get_elements(self.__DESCRIPCION_COBERTURA_VALUE)
        assert len(self.descripcion_cobertura) == 7, "Deberían ser 7"
        assert self.descripcion_cobertura[0].text == "RESPONSABILIDAD CIVIL CON CASCO"
        assert self.descripcion_cobertura[1].text == "D2 - R.C.L. TODO RIESGO - FRANQUICIA FIJA DE $ 9.000.-"
        assert self.descripcion_cobertura[2].text == "DAÑOS POR INCENDIO, TOTAL Y PARCIAL."
        assert self.descripcion_cobertura[3].text == "DAÑOS POR ACCIDENTE, TOTAL Y PARCIAL."
        assert self.descripcion_cobertura[4].text == "ROBO Y/O HURTO, TOTAL Y PARCIAL."
        assert self.descripcion_cobertura[5].text == "GRANIZO, TERREMOTO, INUNDACIÓN, DESBORDAMIENTO,HUELGA Y/O TUMULTO POPULAR."
        assert self.descripcion_cobertura[6].text == "FRANQUICIA FIJA DE $ 9.000.-"

        self.wait_for_element(self.__TIPO_COBERTURA_VALUE)
        self.tipo_cobertura = self.get_elements(self.__TIPO_COBERTURA_VALUE)
        assert len(self.tipo_cobertura) == 7, "Deberían ser 7"
        assert self.tipo_cobertura[0].text == "RC", f"Valor en la UI {self.tipo_cobertura[0].text}"
        assert self.tipo_cobertura[1].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[1].text}"
        assert self.tipo_cobertura[2].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[2].text}"
        assert self.tipo_cobertura[3].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[3].text}"
        assert self.tipo_cobertura[4].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[4].text}"
        assert self.tipo_cobertura[5].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[5].text}"
        assert self.tipo_cobertura[6].text == "CASCO", f"Valor en la UI {self.tipo_cobertura[6].text}"

        self.wait_for_element(self.__LIMITE_VALUE)
        self.limite = self.get_elements(self.__LIMITE_VALUE)
        assert len(self.limite) == 7, "Deberían ser 7"
        assert self.limite[0].text == "0", f"Valor en la UI {self.limite[0].text}"
        assert self.limite[1].text == "0", f"Valor en la UI {self.limite[1].text}"
        assert self.limite[2].text == "0", f"Valor en la UI {self.limite[2].text}"
        assert self.limite[3].text == "0", f"Valor en la UI {self.limite[3].text}"
        assert self.limite[4].text == "0", f"Valor en la UI {self.limite[4].text}"
        assert self.limite[5].text == "0", f"Valor en la UI {self.limite[5].text}"
        assert self.limite[6].text == "0", f"Valor en la UI {self.limite[6].text}"

        self.wait_for_element(self.__FRANQUICIA_VALUE)
        self.franquicia = self.get_elements(self.__FRANQUICIA_VALUE)
        assert len(self.franquicia) == 7, "Deberían ser 7"
        assert self.franquicia[0].text == "0", f"Valor en la UI {self.franquicia[0].text}"
        assert self.franquicia[1].text == "0", f"Valor en la UI {self.franquicia[1].text}"
        assert self.franquicia[2].text == "0", f"Valor en la UI {self.franquicia[2].text}"
        assert self.franquicia[3].text == "0", f"Valor en la UI {self.franquicia[3].text}"
        assert self.franquicia[4].text == "0", f"Valor en la UI {self.franquicia[4].text}"
        assert self.franquicia[5].text == "0", f"Valor en la UI {self.franquicia[5].text}"
        assert self.franquicia[6].text == "0", f"Valor en la UI {self.franquicia[6].text}"

        self.wait_for_element(self.__SUMA_ACCESORIO_VALUE)
        self.suma_accesorio = self.get_elements(self.__SUMA_ACCESORIO_VALUE)
        assert len(self.suma_accesorio) == 7, "Deberían ser 7"
        assert self.suma_accesorio[0].text == "", f"Valor en la UI {self.suma_accesorio[0].text}"
        assert self.suma_accesorio[1].text == "", f"Valor en la UI {self.suma_accesorio[1].text}"
        assert self.suma_accesorio[2].text == "", f"Valor en la UI {self.suma_accesorio[2].text}"
        assert self.suma_accesorio[3].text == "", f"Valor en la UI {self.suma_accesorio[3].text}"
        assert self.suma_accesorio[4].text == "", f"Valor en la UI {self.suma_accesorio[4].text}"
        assert self.suma_accesorio[5].text == "", f"Valor en la UI {self.suma_accesorio[5].text}"
        assert self.suma_accesorio[6].text == "", f"Valor en la UI {self.suma_accesorio[6].text}"
