import logging

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class BuscarPolizasPage(BasePage):

    __PANTALLA_BUSQUEDA_DE_POLIZA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//h5[contains(text(),'Ingrese su búsqueda para visualizar las polizas.')]"
    })

    __INPUT_DE_BUSQUEDA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//input[@class='p-mr-5 p-inputtext p-component ng-untouched ng-pristine ng-invalid']"
    })

    __BUSCAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='p-button p-component']"
    })

    __GRILLA_POLIZA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[contains(@class,'p-flex-column p-mr-2')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PANTALLA_BUSQUEDA_DE_POLIZA)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla de la busqueda de pólizas.")
            return False

    def buscar_poliza_por_numero(self, numero):
        self.wait_for_element(self.__INPUT_DE_BUSQUEDA)
        self.send_keys_to_element(self.__INPUT_DE_BUSQUEDA, numero)

    def buscar_poliza_por_patente(self, numero):
        self.wait_for_element(self.__INPUT_DE_BUSQUEDA)
        self.send_keys_to_element(self.__INPUT_DE_BUSQUEDA, numero)

    def seleccionar_buscar(self):
        self.wait_for_element(self.__BUSCAR)
        self.click_element(self.__BUSCAR)

    def obtener_datos_grilla(self):
        self.wait_for_element(self.__GRILLA_POLIZA)
        self.grilla = self.get_elements(self.__GRILLA_POLIZA)
        return len(self.grilla)

    def  valida_datos_de_poliza_buscada_por_numero(self):
        assert self.grilla[0].text == "Nro de póliza\n513176293", f"{self.grilla[0].text}"
        assert self.grilla[1].text == "Nombre/Razón Social\nBRIANT NICOLAS MATIAS", f"{self.grilla[1].text}"
        assert self.grilla[2].text == "Orden\n0", f"{self.grilla[2].text}"
        assert self.grilla[3].text == "Tipo de movimiento\nPóliza nueva", f"{self.grilla[3].text}"
        assert self.grilla[4].text == "Asegurado\n7925507", f"{self.grilla[4].text}"
        assert self.grilla[5].text == "Vigencia (Desde - Hasta)\n6 oct. 2021 - 6 feb. 2022", f"{self.grilla[5].text}"
        assert self.grilla[6].text == "Fecha emisión\n6 oct. 2021", f"{self.grilla[6].text}"
        assert self.grilla[7].text == "Premio\n4188.18", f"{self.grilla[7].text}"
        assert self.grilla[8].text == "Tipo renovación\nAUTOMATICA", f"{self.grilla[8].text}"

    def valida_datos_de_poliza_buscada_por_patente(self, tipo_de_patente = False):
        if not tipo_de_patente:
            assert self.grilla[0].text == "Nro de póliza\n513176275", f"{self.grilla[0].text}"
            assert self.grilla[1].text == "Nombre/Razón Social\nFIGUEROA ROBERTO JOSE", f"{self.grilla[1].text}"
            assert self.grilla[2].text == "Orden\n0", f"{self.grilla[2].text}"
            assert self.grilla[3].text == "Tipo de movimiento\nPóliza nueva", f"{self.grilla[3].text}"
            assert self.grilla[4].text == "Asegurado\n6644904", f"{self.grilla[4].text}"
            assert self.grilla[5].text == "Vigencia (Desde - Hasta)\n5 oct. 2021 - 5 feb. 2022", f"{self.grilla[5].text}"
            assert self.grilla[6].text == "Fecha emisión\n5 oct. 2021", f"{self.grilla[6].text}"
            assert self.grilla[7].text == "Premio\n7949.34", f"{self.grilla[7].text}"
            assert self.grilla[8].text == "Tipo renovación\nAUTOMATICA", f"{self.grilla[8].text}"
            assert self.grilla[9].text == "Nro de ítem\n1", f"{self.grilla[9].text}"
        else:
            assert self.grilla[0].text == "Nro de póliza\n513176280", f"{self.grilla[0].text}"
            assert self.grilla[1].text == "Nombre/Razón Social\nMARTI FEDERICO", f"{self.grilla[1].text}"
            assert self.grilla[2].text == "Orden\n0", f"{self.grilla[2].text}"
            assert self.grilla[3].text == "Tipo de movimiento\nPóliza nueva", f"{self.grilla[3].text}"
            assert self.grilla[4].text == "Asegurado\n8602853", f"{self.grilla[4].text}"
            assert self.grilla[5].text == "Vigencia (Desde - Hasta)\n5 oct. 2021 - 5 nov. 2021", f"{self.grilla[5].text}"
            assert self.grilla[6].text == "Fecha emisión\n5 oct. 2021", f"{self.grilla[6].text}"
            assert self.grilla[7].text == "Premio\n1020.76", f"{self.grilla[7].text}"
            assert self.grilla[8].text == "Tipo renovación\nAUTOMATICA", f"{self.grilla[8].text}"
            assert self.grilla[9].text == "Nro de ítem\n1", f"{self.grilla[9].text}"

    def seleccionar_poliza(self):
        self.wait_for_element(self.__GRILLA_POLIZA)
        self.click_element(self.__GRILLA_POLIZA)
        self.scroll_to_the_bottom_of_the_page()