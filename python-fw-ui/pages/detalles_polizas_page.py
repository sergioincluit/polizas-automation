import glob
import logging
import os
import time
import urllib.request
from datetime import datetime
from pathlib import Path

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from utils.web_driver_actions import locator_by


class DetallesPolizasPage(BasePage):
    __DETALLE_POLIZA_PAGE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[@class='p-dialog-title ng-tns-c26-1']"
    })

    __LISTAR_BIENES_DESCRIPCION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//thead[@class='p-datatable-thead']//th[contains(text(),'Descripción (Marca, Modelo, Patente)')]"
    })

    __LISTAR_BIENES_UBICACION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//thead[@class='p-datatable-thead']//th[contains(text(),'Ubicación (Domicilio, CP, Localidad)')]"
    })

    __LISTAR_BIENES_SUMA_ASEGURADA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//thead[@class='p-datatable-thead']//th[contains(text(),'Suma asegurada')]"
    })

    __LISTAR_BIENES_VIGENCIA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//thead[@class='p-datatable-thead']//th[contains(text(),'Vigencia (desde y hasta)')]"
    })

    __DATOS_DELPRIMER_BIEN = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//tbody[@class='p-datatable-tbody']//td"
    })

    __LISTADO_DE_BIENES = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-datatable-striped p-datatable p-component']"
    })

    __BOTON_DE_DESCARGA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='p-button-rounded float-right p-button p-component p-button-icon-only']//span[@class='p-button-icon pi pi-download']"
    })

    __PRIMA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'primaText'
    })

    __PRIMA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'primaValue'
    })

    __RECARGO_FINANCIERO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'recargoFinancieroText'
    })

    __RECARGO_FINANCIERO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'recargoFinancieroValue'
    })

    __RECARGO_ADMINISTRATIVO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'recargoAdministrativoText'
    })

    __RECARGO_ADMINISTRATIVO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'recargoAdministrativoValue'
    })

    __SELLADO_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'selladoText'
    })

    __SELLADO_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'selladoValue'
    })

    __IMPUESTOS_INTERNOS_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'impuestosInternosText'
    })

    __IMPUESTOS_INTERNOS_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'impuestosInternosValue'
    })

    __OTROS_IMPUESTOS_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'otrosImpuestosText'
    })

    __OTROS_IMPUESTOS_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'otrosImpuestosValue'
    })

    __TOTAL_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'totalText'
    })

    __TOTAL_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'totalValue'
    })

    __GASTO_EXPLOTACION_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gastoExplotacionText'
    })

    __GASTO_EXPLOTACION_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'gastoExplotacionValue'
    })

    __TASA_UNIFORME_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tasaUniformeText'
    })

    __TASA_UNIFORME_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'tasaUniformeValue'
    })

    __SERVICIOS_SOCIALES_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'serviciosSocialesText'
    })

    __SERVICIOS_SOCIALES_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'serviciosSocialesValue'
    })

    __IVA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'ivaText'
    })

    __IVA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'ivaValue'
    })

    __PERCEPCION_IVA_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'percepcionIvaText'
    })

    __PERCEPCION_IVA_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'percepcionIvaValue'
    })

    __PERCEPCION_IIBB_TEXT = locator_by({
        'BY': By.ID,
        'LOCATOR': 'percepcionIIBBText'
    })

    __PERCEPCION_IIBB_VALUE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'percepcionIIBBValue'
    })

    __VISUALIZAR_DATOS_VEHICULO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='p-button-rounded p-button p-component p-button-icon-only']//span[@class='p-button-icon pi pi-eye']"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__DETALLE_POLIZA_PAGE)
            return True
        except:
            logging.ERROR("No se visualiza la pantalla del Home Page.")
            return False

    def listar_bienes(self):
        self.scroll_to_element(self.__LISTADO_DE_BIENES)
        self.bienes = self.get_elements(self.__DATOS_DELPRIMER_BIEN)
        assert self.bienes[0].text == "MERC.BENZ VITO 111 CDI FURGON V2 AA", "La descripción no concuerda"
        assert self.bienes[1].text == ",1714,ITUZAINGO", "La ubicación no concuerda"
        assert self.bienes[2].text == "935000", "La suma asegurada no concuerda"
        assert self.bienes[3].text == "14 dic. 2018 - 11 ene. 2019", "La vigencia no concuerda"

    def descargar_pdf(self):
        self.scroll_to_element(self.__BOTON_DE_DESCARGA)
        self.click_element(self.__BOTON_DE_DESCARGA)
        time.sleep(5)

    def validar_prima(self):
        self.scroll_to_element(self.__TOTAL_VALUE)
        self.wait_for_element(self.__PRIMA_TEXT)
        assert self.is_text_equal(self.get_text_element(self.__PRIMA_TEXT), "Prima:"), \
            f"El titulo 'Prima:' no coincide con " \
            f"{self.get_text_element(self.__PRIMA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__PRIMA_VALUE), "-6075.46"), \
            f"El valor de Prima '-6075.46' no coincide con " \
            f"{self.get_text_element(self.__PRIMA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__RECARGO_FINANCIERO_TEXT), "Recargo Financiero:"), \
            f"El titulo 'Recargo Financiero:' no coincide con " \
            f"{self.get_text_element(self.__RECARGO_FINANCIERO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__RECARGO_FINANCIERO_VALUE), "-60.75"), \
            f"El valor de Recargo Financiero '-60.75' no coincide con " \
            f"{self.get_text_element(self.__RECARGO_FINANCIERO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__RECARGO_ADMINISTRATIVO_TEXT),
                                  "Recargo Administrativo:"),\
            f"El titulo 'Recargo Administrativo:' no coincide con " \
            f"{self.get_text_element(self.__RECARGO_ADMINISTRATIVO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__RECARGO_ADMINISTRATIVO_VALUE), "-1458.11"), \
            f"El valor de Recargo Administrativo '-1458.11' no coincide con " \
            f"{self.get_text_element(self.__RECARGO_ADMINISTRATIVO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__SELLADO_TEXT), "Sellado:"), \
            f"El titulo 'Sellado:' no coincide con " \
            f"{self.get_text_element(self.__SELLADO_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__SELLADO_VALUE), "0"), \
            f"El valor de Sellado '0' no coincide con " \
            f"{self.get_text_element(self.__SELLADO_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__IMPUESTOS_INTERNOS_TEXT), "Impuestos Internos:"), \
            f"El titulo 'Impuestos Internos:' no coincide con " \
            f"{self.get_text_element(self.__IMPUESTOS_INTERNOS_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__IMPUESTOS_INTERNOS_VALUE), "-8.22"), \
            f"El valor de Impuestos Internos '-8.22' no coincide con " \
            f"{self.get_text_element(self.__IMPUESTOS_INTERNOS_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__OTROS_IMPUESTOS_TEXT), "Otros Impuestos:"), \
            f"El titulo 'Otros Impuestos:' no coincide con " \
            f"{self.get_text_element(self.__OTROS_IMPUESTOS_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__OTROS_IMPUESTOS_VALUE), "-82.02"), \
            f"El valor de Otros Impuestos '-82.02' no coincide con " \
            f"{self.get_text_element(self.__OTROS_IMPUESTOS_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__TOTAL_TEXT), "Total:"), \
            f"El titulo 'Total:' no coincide con " \
            f"{self.get_text_element(self.__TOTAL_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__TOTAL_VALUE), "-11006.91"), \
            f"El valor de Total '-11006.91' no coincide con " \
            f"{self.get_text_element(self.__TOTAL_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__GASTO_EXPLOTACION_TEXT), "Gasto de explotación:"), \
            f"El titulo 'Gasto de explotación:' no coincide con " \
            f"{self.get_text_element(self.__GASTO_EXPLOTACION_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__GASTO_EXPLOTACION_VALUE), "-607.54"), \
            f"El valor de Gasto de explotación '-607.54' no coincide con " \
            f"{self.get_text_element(self.__GASTO_EXPLOTACION_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__TASA_UNIFORME_TEXT), "Tasa Uniforme:"), \
            f"El titulo 'Tasa Uniforme:' no coincide con " \
            f"{self.get_text_element(self.__TASA_UNIFORME_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__TASA_UNIFORME_VALUE), "-49.21"), \
            f"El valor de Tasa Uniforme '-49.21' no coincide con " \
            f"{self.get_text_element(self.__TASA_UNIFORME_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__SERVICIOS_SOCIALES_TEXT), "Servicios Sociales:"), \
            f"El titulo 'Servicios Sociales:' no coincide con " \
            f"{self.get_text_element(self.__SERVICIOS_SOCIALES_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__SERVICIOS_SOCIALES_VALUE), "-41.01"), \
            f"El valor de Servicios Sociales '-41.01' no coincide con " \
            f"{self.get_text_element(self.__SERVICIOS_SOCIALES_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__IVA_TEXT), "IVA:"), \
            f"El titulo 'IVA:' no coincide con " \
            f"{self.get_text_element(self.__IVA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__IVA_VALUE), "-1722.39"), \
            f"El valor de IVA '-1722.39' no coincide con " \
            f"{self.get_text_element(self.__IVA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__PERCEPCION_IVA_TEXT), "Percepción IVA:"), \
            f"El titulo 'Percepción IVA:' no coincide con " \
            f"{self.get_text_element(self.__PERCEPCION_IVA_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__PERCEPCION_IVA_VALUE), "-246.05"), \
            f"El valor de Percepción IVA '-246.05' no coincide con " \
            f"{self.get_text_element(self.__PERCEPCION_IVA_VALUE)}"

        assert self.is_text_equal(self.get_text_element(self.__PERCEPCION_IIBB_TEXT), "Percepción IIBB:"), \
            f"El titulo 'Percepción IIBB:' no coincide con " \
            f"{self.get_text_element(self.__PERCEPCION_IIBB_TEXT)}"
        assert self.is_text_equal(self.get_text_element(self.__PERCEPCION_IIBB_VALUE), "-656.15"), \
            f"El valor de Percepción IIBB '-656.15' no coincide con " \
            f"{self.get_text_element(self.__PERCEPCION_IIBB_VALUE)}"

    def visualizar_datos_del_vehiculo(self):
        self.wait_for_element(self.__VISUALIZAR_DATOS_VEHICULO)
        self.click_element(self.__VISUALIZAR_DATOS_VEHICULO)
