import logging
import allure
import pytest
from assertpy import assert_that
from apis.api_poliza_qa import ApiPolizaQA

pytestmark = [
    allure.parent_suite('API'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('-')
]


@pytest.mark.API
@pytest.mark.regression
class TestPolizaQA:
    def setup_class(self):
        logging.info("Llamando al constructor de la API en qa")
        self.api_poliza = ApiPolizaQA("testpol@lamercantilandina.com.ar", "R2GtN1*WVAi5")

    def test_mostrar_poliza_por_numero(self):
        logging.info("Llamando a la función que busca una póliza por número en qa")
        response = self.api_poliza.get_buscar_poliza_por_numero(512836962)
        assert_that(response.status_code).is_equal_to(200)
