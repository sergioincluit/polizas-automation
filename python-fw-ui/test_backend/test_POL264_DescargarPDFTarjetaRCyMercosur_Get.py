import datetime
import glob
import logging
import os

import allure
import pytest

from assertpy import assert_that
from pathlib import Path
from utils.pdf_utils import UtilsPDF
from apis.api_poliza_qa import ApiPolizaQA

pytestmark = [
    allure.parent_suite('API'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('-')
]


@pytest.mark.API
@pytest.mark.regression
class TestDescargarPDFTarjetaRCyMercosur:
    def setup_class(self):
        logging.info("Llamando al constructor de la API en qa")
        self.api_poliza = ApiPolizaQA("testpol@lamercantilandina.com.ar", "R2GtN1*WVAi5")

    def test_get_pdf_de_tarjetarc_y_mercosur(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.api_poliza.get_pdf_de_tarjetarc_y_mercosur(510693342,4,1)
        assert_that(response.status_code).is_equal_to(200)

    def test_download_pdf_de_tarjetarc_y_mercosur(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.api_poliza.get_pdf_de_tarjetarc_y_mercosur(510693342,4,1)
        path = Path("pdf_POL264")
        path.mkdir(parents=True, exist_ok=True)
        with open(datetime.datetime.now().strftime(f"pdf_POL264/%Y-%m-%d_%H.%M.%S.pdf"), "wb") as code:
            code.write(response.content)
            code.close()
        ruta_pdf = max(glob.iglob('pdf_POL264/*'),key=os.path.getctime)
        number_of_pages = UtilsPDF.return_number_of_pages(ruta_pdf)
        assert_that(number_of_pages).is_equal_to(2)
        page_content_1 = UtilsPDF.return_text_of_pdf(ruta_pdf,0)
        assert_that(page_content_1).contains("ASEGURADORA | SEGURADORA: La Mercantil Andida S.A")
        assert_that(page_content_1).contains("ASEGURADO | SEGURADO: ALVARINO PEDRO ALEJANDRO")
        assert_that(page_content_1).contains("MARCA MODELO AÑO | MARCA MODELO ANO: MERC.BENZ VITO 111 CDI")
        assert_that(page_content_1).contains("Asegurado: ALVARINO PEDRO ALEJANDRO")
        assert_that(page_content_1).contains("Vig. desde 2018-12-14")
        assert_that(page_content_1).contains("Motor: R9MA502C014041")
        assert_that(page_content_1).contains("Asegurado: 7564864")

    def test_get_pdf_de_tarjetarc_y_mercosur_poliza_no_existente(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.api_poliza.get_pdf_de_tarjetarc_y_mercosur(51069334222,4,1)
        assert_that(response.status_code).is_equal_to(404)

    def test_get_pdf_de_tarjetarc_y_mercosur_con_poliza_no_valida(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.api_poliza.get_pdf_de_tarjetarc_y_mercosur("asdasd",4,1)
        assert_that(response.status_code).is_equal_to(400)