import logging
from pprint import pprint

import allure
import pytest
from assertpy import assert_that
from apis.api_poliza_dev import ApiPolizaDEV

pytestmark = [
    allure.parent_suite('API'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('-')
]


@pytest.mark.API
@pytest.mark.regression
class TestPolizadev:
    def setup_class(self):
        logging.info("Llamando al constructor de la API en dev")
        self.api_poliza = ApiPolizaDEV("testpol@lamercantilandina.com.ar", "R2GtN1*WVAi5")

    def test_obtener_poliza_por_numero(self):
        logging.info("Llamando a la función que busca una póliza por número en dev")
        response = self.api_poliza.get_buscar_poliza_por_numero(511796691)
        assert_that(response.status_code).is_equal_to(200)
        pprint(response.json())
        poliza = response.json()
        assert poliza['data'] is not None
        assert poliza['data'][0]['numero'] is not None

